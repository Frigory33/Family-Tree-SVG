# Family Tree SVG

This repository provides a script that is able to generate a nice circular graphical tree using the data of a text file. It is
primarily meant for family trees as offsprings of a root couple.

This script was developed for festivities of the descendants of my mother’s great-grandparents. :)


## Usage

You have to create two files for the script: the tree description as a text file and a SVG template. Then you must launch the
script with filepaths as arguments.

An example of tree files ready for generation is provided in the sample/ directory of the repository.

### The tree description file

It must be a simple plain text file using UTF-8 charset.

You must put in it the description of the family, as a tree of names with indentation using the Tab character, in this form:

```
Root person
Root person’s companion
	Their first child
	Their first child’s wife or husband
		Their grandson

	Their second child
```

As shown above:
- you must add an indentation level (with a Tab) for the children;
- you must put an empty line before each non-first child;
- you may write one person’s name, dates, etc. per line.

Each sequence of lines which have the same indentation level and that are not separated with an empty line or an inferior
indentation level will be in the same section (i.e. tree node) in the generated drawing. People who are grouped in a section are
meant to be a couple (or maybe more people who have had crazy love together) who possibly got children.

You may put line breaks even when you’re not writing a new name, to add a line break in the drawing (e.g. so that the text doesn’t
overflow from the section, as it is done in the sample), but this makes some other functionality unusable.

You can put a minus sign (`−`, U+2212) followed by a space before any line of a section to indicate that the people in the couple
(or the more exotic mix) are separated. Their section will be broken in the drawing.

You can put a line with a plus sign (`+`) before a section, with the same indentation level, so that the previous section is linked
to it. It allows to show that someone had a companion, quitted it and is now with someone else:

```
Parent
	Child
	− First companion
	+
	Child (the same one)
	Current companion
```

Putting several root sections in your tree description may work, but then the font size will probably be too big. To adjust the
font sizes, you have to modify the script or the resulting SVG.

You can indicate the color or color range of a section as hexadecimal codes on one (usually the first) line. The color will be
transmitted to the non-manually colored children. If a root section is not assigned a color or color range, it uses `808080–E8E8E8`
(middle gray – light gray) as default. If a section is assigned a color range, it uses the middle color of it, and divides it for
its children so that each children has a range of the same size (± 1 because of rounds) as its brothers and sisters. The two
hexadecimal codes of the range must be bound by an en dash (`–`, U+2013). In the example below:

```
Root
	Leaf 1

	Leaf 2
```

* Root will have the range `808080–E8E8E8` and the color `B4B4B4`;
* Leaf 1 will have the range `808080–B4B4B4` and the color `9A9A9A`;
* Leaf 2 will have the range `B4B4B4—E8E8E8` and the color `CDCDCD`.

### The SVG template

The script will take this as tags surrounding the drawing of the tree. It should be indented with tabs to be in accordance with the
text generated by the script. You must put a line containing `{FamilyTree}`, with nothing else than this tag and indentation, to
indicate that the script should put his SVG code here.

The SVG width and height will be taken in account by the script: the drawing will be inscribed in the defined page. The attributes
must be on the same line as the opening `<svg` tag to be read by the script; the script does not parse the XML, it simply uses a
hard-coded regex, that’s why it’s restrictive. If this doesn’t answer your needs, you may modify the script, edit the resulting SVG
or use the SVG `viewBox` attribute in the template’s root tag. By default, the drawing uses a diameter of 1000.

You may use this template functionality to add scripts with your tree to make it interactive. Indeed the provided sample includes
a script that allows to rotate the tree using the mouse wheel when the Ctrl and Shift keys are pressed.

### The generation command

```sh
./make-svg.py tree-desc.txt template.svg result.svg
```

You may call Python yourself:

```sh
python make-svg.py tree-desc.txt template.svg result.svg
```

As shown in the sample directory, you may put the command in a file if you need to repeat editing your generation files and
generating the drawing.
