# Arbre généalogique en SVG

Ce dépôt fournit un script capable de générer un élégant arbre généalogique circulaire en utilisant les données d’un fichier texte.
Il est fait en premier lieu pour les arbres généalogiques constitués de la descendance d’un couple racine.

Ce script a été développé pour les cousinades des descendants des arrière-grands-parents de ma mère. :)


## Utilisation

Le script a besoin de deux fichiers : un fichier texte décrivant l’arbre et un patron de fichier SVG. Le script doit être lancé
avec les chemins d’accès à ces fichiers en tant que paramètres une fois qu’ils sont prêts.

Un exemple de fichiers prêts pour la génération de l’arbre est proposé dans le répertoire sample/ du dépôt.

### Le fichier texte décrivant l’arbre

Le contenu du fichier doit être du texte brut encodé en UTF-8.

La famille est décrite par un arbre de noms avec de l’indentation utilisant le caractère tabulation, ayant la forme suivante :

```
Personne racine
Compagne ou compagnon de la personne racine
	Leur premier enfant
	La femme ou le mari de leur premier enfant
		Leur petit-fils

	Leur deuxième enfant
```

Comme montré ci-dessus :
- il faut ajouter un niveau d’indentation (avec une tabulation) pour passer aux enfants ;
- il faut faire précéder la description de chaque nouvel enfant (qui n’est pas le premier de la fratrie) par une ligne vide ;
- il faut préférentiellement écrire le détail (nom, dates, etc.) d’une personne par ligne.

Chaque séquence de lignes qui ont le même niveau d’indentation et qui ne sont pas séparées par une ligne vide ou un niveau
d’indentation inférieur seront placées dans la même section (c’est-à-dire le même nœud de l’arbre) dans le dessin généré, l’idée
étant qu’il s’agit d’un couple (ou même de davantage de personnes ayant connu l’amour fou toutes ensemble) ayant éventuellement eu
des enfants.

Il est possible de revenir à la ligne même si ce n’est pas pour décrire une nouvelle personne, afin que ce retour à la ligne
soit également réalisé sur le dessin (notamment afin que le texte ne dépasse pas de la section, comme cela est fait dans
l’exemple), mais cela rend certaines fonctionnalités inutilisables.

Un signe moins (`−`, U+2212) suivi d’une espace au début de n’importe quelle ligne d’une section indique que les deux personnes du
couple (ou les multiples personnes de la section) sont séparées. Leur section sera brisée dans le dessin.

Deux sections peuvent être liées en mettant une ligne ayant pour simple contenu un signe plus (`+`) avant une section avec le même
niveau d’indentation que celle-ci. Cela permet d’afficher qu’un enfant a eu un premier conjoint ou une première conjointe, l’a
quitté·e et est avec quelqu’un d’autre depuis :

```
Parent
	Enfant
	− Première compagne ou compagnon
	+
	Enfant (le même)
	Compagne ou compagnon actuel
```

Un arbre comportant plusieurs sections en racine devrait être dessiné correctement, mais la police de caractères sera alors
probablement trop grande. Pour ajuster la taille des polices, vous devez modifier le script ou le SVG obtenu.

La couleur ou l’intervalle de couleurs d’une section doit être indiqué avec des codes hexadécimaux sur une (en général, la
première) ligne de la section. Si c’est une simple couleur, elle sera transmise aux enfants auxquels aucune couleur n’a été
affectée manuellement. Si aucune couleur n’est donnée à la section racine, elle utilise par défaut l’intervalle `808080–E8E8E8`
(gris moyen – gris clair). Si un intervalle de couleur est donné à une section, celle-ci prendra la couleur située au milieu de cet
intervalle, et le divisera pour ses enfants afin que chacun d’entre eux reçoive un intervalle de la même taille (± 1 à cause des
arrondis). Les deux codes hexadécimaux de l’intervalle doivent être liés par un tiret demi-cadratin (`–`, U+2013). Dans l’exemple
ci-dessous :

```
Racine
	Feuille 1

	Feuille 2
```

* Racine obtiendra l’intervalle `808080–E8E8E8` et la couleur `B4B4B4`;
* Feuille 1 obtiendra l’intervalle `808080–B4B4B4` et la couleur `9A9A9A`;
* Feuille 2 obtiendra l’intervalle `B4B4B4—E8E8E8` et la couleur `CDCDCD`.

### Le SVG patron

Le script utilisera ce patron comme balises entourant celles du dessin de l’arbre. Le code du patron doit utiliser le caractère
tabulation pour son indentation afin de s’accorder avec le texte généré par le script. Le patron doit contenir une ligne ayant pour
contenu `{FamilyTree}`, sans rien d’autre que cette balise et l’indentation, afin d’indiquer que le script doit placer son code SVG
à cet endroit.

La largeur et la hauteur du document SVG (attributs `width` and `height`) sont pris en compte par le script : le dessin sera
inscrit dans la page définie. Les attributs doivent être sur la même ligne que le début de la balise `<svg` afin d’être cernés par
le script ; celui-ci n’analyse pas le code XML, il utilise simplement une expression rationnelle bidouillée, d’où la restriction.
Si cela ne répond pas à vos besoins, vous pouvez modifier le script, modifier le fichier SVG qu’il a produit ou utiliser l’attribut
SVG `viewBox` dans la balise racine de votre patron. Par défaut, le dessin utilise un diamètre de 1000.

Cette fonctionnalité du patron peut par exemple être utilisée pour insérer des scripts avec votre arbre afin de le rendre
interactif. L’exemple fourni inclut en effet un script qui permet de pivoter l’arbre en utilisant la molette de la souris lorsque
les touches Ctrl et Maj sont enfoncées.

### La commande de génération

```sh
./make-svg.py arbre.txt patron.svg resultat.svg
```

Ou en appelant Python soi-même :

```sh
python make-svg.py arbre.txt patron.svg resultat.svg
```

Il est recommandé d’écrire cette commande dans un fichier exécutable si vous devez l’exécuter de nombreuses fois avec les mêmes
noms de fichiers. Un tel fichier est présent dans le dossier d’exemple.
