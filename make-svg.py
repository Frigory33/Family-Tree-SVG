#! /bin/env python3
# -*- encoding: utf-8 -*-


from sys import argv
from math import *
import re


class FamilyTree:
   def __init__(self, parent = None, level = -1):
      self.parent = parent
      self.level = level
      self.colorRange = []
      self.color = None
      self.names = []
      self.children = []
      self.broken = False
      self.linkedToNext = False

   def print(self):
      if self.colorRange:
         print(self.level * "\t", "–".join(self.colorRange), sep = "")
      if self.names:
         for name in self.names[:-1]:
            print(self.level * "\t", name, sep = "")
         print(self.level * "\t", "− " if self.broken else "", self.names[-1], sep = "")
      for child in self.children:
         child.print()
         if child != self.children[-1]:
            print("+" if self.linkedToNext else "")

   def countLeafs(self):
      if self.children:
         self.leafsCount = 0
         for child in self.children:
            child.countLeafs()
            self.leafsCount += child.leafsCount
      else:
         self.leafsCount = 1

   def getMaxLevel(self):
      self.maxLevel = self.level + 1
      for child in self.children:
         child.getMaxLevel()
         self.maxLevel = max(child.maxLevel, self.maxLevel)

   def calcAnglePart(self):
      self.anglePart = 0
      for child in self.children:
         child.calcAnglePart()
         self.anglePart += child.anglePart
      minAngle = 1 / self.level if self.level != 0 else 1
      self.anglePart = max(self.anglePart, minAngle)

   def formatNames(self):
      for nameI, name in enumerate(self.names):
         if name.startswith("− "):
            self.broken = True
            self.names[nameI] = name = name[2:]
      for child in self.children:
         child.formatNames()

   def setColors(self):
      def colorStrToComponents(colorStr):
         color = int(colorStr, 16)
         return [(color >> (8 * compI)) & 0xFF for compI in range(2, -1, -1)]
      def colorComponentsToStr(components):
         return "".join("{:02X}".format(int(round(comp))) for comp in components)

      if len(self.colorRange) == 1:
         self.colorRange = [self.colorRange[0]] * 2
      elif not self.colorRange:
         self.colorRange = ['808080', 'E8E8E8']
      colorRange = list(map(colorStrToComponents, self.colorRange))
      self.color = colorComponentsToStr([(comp1 + comp2) / 2 for comp1, comp2 in zip(*colorRange)])

      color = colorRange[0]
      colorIncr = [(comp2 - comp1) / max(1, len(self.children)) for comp1, comp2 in zip(*colorRange)]
      for child in self.children:
         endColor = [comp1 + comp2 for comp1, comp2 in zip(color, colorIncr)]
         if not child.colorRange:
            child.colorRange = [colorComponentsToStr(colorStop) for colorStop in [color, endColor]]
         child.setColors()
         color = endColor


with open(argv[1] if len(argv) >= 1 else 'tree.txt', 'rt') as treeDescFile:
   familyTree = FamilyTree()
   curNode = familyTree
   nextChild = False
   for line in treeDescFile.readlines():
      content = line.strip()
      if not content:
         nextChild = True
      else:
         level = 0
         while line[level].isspace():
            level += 1
         while curNode.level > level:
            curNode = curNode.parent
         if content == "+":
            curNode.linkedToNext = True
            nextChild = True
         else:
            if nextChild and curNode.parent:
               curNode = curNode.parent
            if level > curNode.level:
               curNode.children.append(FamilyTree(curNode, level))
               curNode = curNode.children[-1]
            if re.match(r"[\da-fA-F]{6}(–[\da-fA-F]{6})?", content):
               curNode.colorRange = content.split("–")
            else:
               curNode.names.append(content)
            nextChild = False

familyTree.getMaxLevel()
familyTree.calcAnglePart()
familyTree.formatNames()
familyTree.setColors()


with open(argv[2] if len(argv) >= 2 else 'template.svg', 'rt') as templateFile, \
   open(argv[3] if len(argv) >= 3 else 'tree.svg', 'wt') as treeSvgFile:

   indentLevel = 0
   def print(line):
      treeSvgFile.write(indentLevel * '\t' + line + '\n')
   def printIndent(line):
      global indentLevel
      print(line)
      indentLevel += 1
   def printUnindent(line):
      global indentLevel
      indentLevel -= 1
      print(line)

   takenIds = {}
   def getNewId(prefix):
      takenIds.setdefault(prefix, 0)
      takenIds[prefix] += 1
      return prefix + str(takenIds[prefix])

   def modAngle(angle):
      return fmod(fmod(angle, pi * 2) + pi * 2, pi * 2)
   def rad2deg(angle):
      return modAngle(angle) * 180 / pi

   sideWidth = 1000
   centerX = centerY = sideWidth / 2

   LEVEL_PART = .98
   sectionTextSize = sideWidth / 36 / familyTree.maxLevel

   def getIntermAngle(level):
      return pi / 30 / (2 ** level)
   def getRadius(level):
      return level * sideWidth / 2 / familyTree.maxLevel
   def getCoords(angle, radius):
      return (centerX + cos(angle) * radius, centerY + sin(angle) * radius)

   def printRoot(angle1, angle2, color, texts):
      printIndent('<g>')

      outerRadius = getRadius(LEVEL_PART)
      thickness = outerRadius / 4
      textPathRadius = outerRadius - thickness / 2

      def getShapeString(radius, inverted, line = False):
         angles = (angle2, angle1) if inverted else (angle1, angle2)
         moreThanHalf = 0 if abs(angle2 - angle1) < pi else 1
         negativeStart = 0 if inverted else 1
         return '{7} {1:.2f},{2:.2f} A {0:.2f},{0:.2f} 0 {5} {6} {3:.2f},{4:.2f}' \
            .format(radius, *getCoords(angles[0], radius), *getCoords(angles[1], radius), moreThanHalf, negativeStart,
               'L' if line else 'M')

      # Textpath’s side attribute works in Firefox but works neither in Inkscape (version 0.92) nor in Chromium,
      #    therefore we define two paths
      pathId, oppositePathId = getNewId('rootCircle'), getNewId('oppositeRootCircle')
      printIndent('<defs data-angle1="{:.2f}" data-angle2="{:.2f}">'.format(rad2deg(angle1), rad2deg(angle2)))
      print('<path id="{}" d="{}" />'.format(pathId, getShapeString(textPathRadius, False)))
      print('<path id="{}" d="{}" />'.format(oppositePathId, getShapeString(textPathRadius, True)))
      printUnindent('</defs>')

      print('<path d="{} {} Z" style="fill: {};" />'
         .format(getShapeString(outerRadius, False), getShapeString(outerRadius - thickness, True, True), color))

      textSize = thickness * .5
      for textI, text in enumerate(texts):
         printIndent('<text style="font-size: {:.2f}px;">'.format(textSize))
         refPathId = pathId
         offset = 1 - (textI * 2 + 1) / len(texts) / 2
         textAngle = modAngle(angle1 + (angle2 - angle1) * offset)
         if textAngle < pi:
            refPathId = oppositePathId
            offset = 1 - offset
         print('<textPath href="#{}" startOffset="{:.2f}%"><tspan dy="{:.2f}">{}</tspan></textPath>'
            .format(refPathId, offset * 100, textSize * .4, text))
         printUnindent('</text>')

      printUnindent('</g>')

   def printSection(level, angle1, angle2, color, texts, broken, linkedToNext):
      if linkedToNext:
         printIndent('<g class="notCurrent">')
         radius = getRadius(level + LEVEL_PART / 2)
         print('<line x1="{:.2f}" y1="{:.2f}" x2="{:.2f}" y2="{:.2f}" style="stroke-width: {:.2f}px; stroke: {};" />'
            .format(*getCoords(angle2, radius), *getCoords(angle2 + getIntermAngle(level), radius),
               sideWidth / 20 / familyTree.maxLevel, color))
      else:
         printIndent('<g>')

      def getShapeValues(level, angle1, angle2):
         moreThanHalf = 0 if abs(angle2 - angle1) < pi else 1
         radius = getRadius(level)
         return (radius, *getCoords(angle1, radius), *getCoords(angle2, radius), moreThanHalf)

      lineHeight = sectionTextSize * 1.15

      if broken:
         midAngle, breakAngle = (angle1 + angle2) / 2, pi / 120 / level / familyTree.maxLevel
         midAngle1, midAngle2 = midAngle - breakAngle / 2, midAngle + breakAngle / 2
         r1, r2 = getRadius(level), getRadius(level + LEVEL_PART)
         shiftsCount = 40
         shiftAngle, shiftWidth = midAngle - pi / 4, ((((r2 - r1) / shiftsCount) ** 2) / 2) ** .5
         shiftX1, shiftY1 = cos(shiftAngle) * shiftWidth, sin(shiftAngle) * shiftWidth
         shiftX2, shiftY2 = cos(shiftAngle + pi / 2) * shiftWidth, sin(shiftAngle + pi / 2) * shiftWidth
         shape1 = \
            'M {1:.2f},{2:.2f} A {0:.2f},{0:.2f} 0 {5} 1 {3:.2f},{4:.2f}'.format(*getShapeValues(level, angle1, midAngle1)) + \
            ' l {:.2f},{:.2f} l {:.2f},{:.2f}'.format(shiftX1, shiftY1, shiftX2, shiftY2) * shiftsCount + \
            ' A {0:.2f},{0:.2f} 0 {5} 0 {1:.2f},{2:.2f} Z'.format(*getShapeValues(level + LEVEL_PART, angle1, midAngle1))
         shape2 = \
            'M {1:.2f},{2:.2f} A {0:.2f},{0:.2f} 0 {5} 0 {3:.2f},{4:.2f}'.format(*getShapeValues(level, angle2, midAngle2)) + \
            ' l {:.2f},{:.2f} l {:.2f},{:.2f}'.format(shiftX1, shiftY1, shiftX2, shiftY2) * shiftsCount + \
            ' A {0:.2f},{0:.2f} 0 {5} 1 {1:.2f},{2:.2f} Z'.format(*getShapeValues(level + LEVEL_PART, angle2, midAngle2))
         print('<path d="{}" style="fill: {};" />'.format(shape1, color))
         print('<path d="{}" style="fill: {};" />'.format(shape2, color))
         lineHeight *= 1.5
      else:
         shape = 'M {1:.2f},{2:.2f} A {0:.2f},{0:.2f} 0 {5} 1 {3:.2f},{4:.2f}'.format(*getShapeValues(level, angle1, angle2))
         shape += ' L {3:.2f},{4:.2f} A {0:.2f},{0:.2f} 0 {5} 0 {1:.2f},{2:.2f} Z' \
            .format(*getShapeValues(level + LEVEL_PART, angle1, angle2))
         print('<path d="{}" style="fill: {};" />'.format(shape, color))

      textAngle = (angle1 + angle2) / 2
      actualTextAngle = textAngle + (0 if modAngle(textAngle) < pi / 2 or pi * 3 / 2 <= modAngle(textAngle) else pi)
      textX, textY = getCoords(textAngle, getRadius(level + LEVEL_PART / 2))
      textTop = textY - (len(texts) + .5) * lineHeight / 2
      printIndent(('<text x="{0:.2f}" y="{3:.2f}" transform="rotate({2:.2f} {0:.2f} {1:.2f})">')
            .format(textX, textY, rad2deg(actualTextAngle), textTop))
      for textI, text in enumerate(texts):
         print('<tspan x="{:.2f}" dy="{:.2f}">{}</tspan>'.format(textX, lineHeight, text))
      printUnindent('</text>')

      printUnindent('</g>')

   def printFamilySvg(node = familyTree, startAngle = 0, angleIncr = pi * 2):
      if (node.parent and node.children) or len(node.children) >= 2:
         printIndent('<g class="branch">')

      if node.parent:
         intermAngle = getIntermAngle(node.level)
         angleIncr -= intermAngle
         if node.level == 0:
            startAngle += intermAngle / 2
            printRoot(startAngle, startAngle + angleIncr, '#' + node.color, node.names)
         else:
            printSection(node.level, startAngle, startAngle + angleIncr,
               '#' + node.color, node.names, node.broken, node.linkedToNext)
         angleIncr += getIntermAngle(node.level + 1)

      angle = startAngle + angleIncr * (1 - sum(child.anglePart for child in node.children) / node.anglePart) / 2
      for child in node.children:
         subAngleIncr = angleIncr * child.anglePart / node.anglePart
         printFamilySvg(child, angle, subAngleIncr)
         angle += subAngleIncr

      if (node.parent and node.children) or len(node.children) >= 2:
         printUnindent('</g>')

   def printSvg():
      global indentLevel
      treeString = '{FamilyTree}'
      svgDimensions = None
      for line in templateFile.readlines():
         if line.strip() == treeString:
            indentLevel = line.index(treeString)
            printIndent('<style>')
            printIndent('text {')
            print('fill: black;');
            print('font-family: sans-serif;')
            print('font-size: {:.2f}px;'.format(sectionTextSize));
            print('text-anchor: middle;')
            printUnindent('}')
            printIndent('line {')
            print('stroke-linecap: square;')
            printUnindent('}')
            printUnindent('</style>')
            printFamilySvg()
            indentLevel = 0
         else:
            if not svgDimensions:
               svgDimensions = re.search(r'<svg (.+ )?width="(?P<width>\d+)" (.+ )?height="(?P<height>\d+)"', line)
               if svgDimensions:
                  width, height = map(int, svgDimensions.group('width', 'height'))
                  global sideWidth, centerX, centerY
                  sideWidth = min(width, height)
                  centerX, centerY = width / 2, height / 2
            print(line.rstrip())

   printSvg()
